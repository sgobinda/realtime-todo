import React from 'react';
import { Route, Switch } from 'react-router';

import Header from '../modules/common/Header';
import Footer from '../modules/common/Footer';
import NoMatch from '../modules/common/NoMatch';

import Login from '../modules/Login';
import Home from '../modules/Home';
import Home2 from '../modules/Home2';

export default (
  <div>
    <Header />
      <Switch>
        <Route exact path="/" component={Login} />
          <Route exact path="/home" component={Home} />
          <Route exact path="/home2" component={Home2} />
          <Route component={NoMatch} />
      </Switch>
    <Footer />
  </div>
);
