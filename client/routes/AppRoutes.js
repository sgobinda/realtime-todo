import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Public from './Public';
import Authenticated from './Authenticated';
import AppNavigation from '../modules/navigations/AppNavigation';
import LoginContainer from '../containers/LoginContainer';
import Home from '../modules/Home';
import Home2 from '../modules/Home2';
import NoMatch from '../modules/common/NoMatch';
import Footer from '../modules/common/Footer';
import IndexPage from '../modules';

class AppRoutes extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
    <Router>
    <div>
      <AppNavigation {...this.props} />
        <Switch>
          <Route exact name="index" path="/" component={IndexPage} />
          <Authenticated exact path="/home" component={Home} {...this.props} />
          <Authenticated exact path="/home2" component={Home2} {...this.props} />
          <Public path="/login" component={LoginContainer} {...this.props} />
          <Route component={NoMatch} />
        </Switch>
      <Footer />
      </div>
    </Router>
    )
  }
}

AppRoutes.propTypes = {
  loggingIn: PropTypes.bool,
  authenticated: PropTypes.bool,
};
const mapStateToProps = state => {
  return {loggingIn: state.auth.loggingIn, authenticated: state.auth.authenticated}
}

export default connect(mapStateToProps)(AppRoutes);
