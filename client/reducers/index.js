import {combineReducers} from 'redux';
import testReducer from './testReducer';
import auth from './authReducer';

const rootReducer = combineReducers({
  testReducer,
  auth
})

export default rootReducer;
