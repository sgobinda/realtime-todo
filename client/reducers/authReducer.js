import {
  SIGN_IN_REQUEST,
  SIGN_OUT_REQUEST
} from '../actions/actionTypes';

const initialState = {
  loggingIn: false,
  authenticated: false,
}
export default function authReducer(state=initialState, action) {
  switch(action.type) {
    case SIGN_IN_REQUEST:
      return {
        ...state,
        loggingIn: action.payload,
        authenticated: action.payload
      };
      case SIGN_OUT_REQUEST:
        return {
          ...state,
          loggingIn: action.payload,
          authenticated: action.payload
        };
    default:
      return state
  }
}
