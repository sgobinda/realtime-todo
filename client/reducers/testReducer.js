import { TEST } from '../actions/actionTypes';

export default function testReducer(state={}, action) {
  switch(action.type) {
    case TEST:
      return action.payload;
    default:
      return state
  }
}
