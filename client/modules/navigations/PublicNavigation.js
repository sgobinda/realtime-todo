import React, { Component } from 'react';
import { ToolbarGroup, ToolbarTitle} from 'material-ui/Toolbar';
import RaisedButton from 'material-ui/RaisedButton';
import { NavLink } from 'react-router-dom';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import AppBar from 'material-ui/AppBar';

class PublicNavigation extends Component {

  constructor(props) {
    super(props);
    this.handleClose = this.handleClose.bind(this);
  }
  handleClose() {
    this.props.handleClose();
  }
  render() {
    const { status } = this.props;
    return(
      <Drawer
          docked={false}
          width={200}
          open={status}
          onRequestChange={this.handleClose}
        >
          <AppBar title="Menu" iconElementLeft={<span/>}/>
          <NavLink to="/login"><MenuItem onTouchTap={this.handleClose}>Login</MenuItem></NavLink>
          <NavLink to="/"><MenuItem onTouchTap={this.handleClose}>Sign Up</MenuItem></NavLink>
        </Drawer>
    )
  }
}
export default PublicNavigation;
