import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import AppBar from 'material-ui/AppBar';

class AuthenticatedNavigation extends Component {

  constructor(props) {
    super(props);
    this.handleClose = this.handleClose.bind(this);
  }

  handleClose() {
    this.props.handleClose();
  }

  render() {
    const { status } = this.props;
    return(
      <Drawer
          docked={false}
          width={200}
          open={status}
          onRequestChange={this.handleClose}
        >
          <AppBar title="Menu" iconElementLeft={<span/>}/>
          <Link to="/home"><MenuItem onTouchTap={this.handleClose}>Home</MenuItem></Link>
          <Link to="/home2"><MenuItem onTouchTap={this.handleClose}>Home2</MenuItem></Link>
        </Drawer>
    )
  }
}
export default AuthenticatedNavigation;
