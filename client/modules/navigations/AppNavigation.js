import React, { Component } from 'react';
import PublicNavigation from './PublicNavigation';
import AuthenticatedNavigation from './AuthenticatedNavigation';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationApps from 'material-ui/svg-icons/navigation/apps';

const styles = {
  barStyle: {
    borderRadius: "0px",
    position: "fixed",
    zIndex: 1101,
    width: "100%",
    display: "flex",
    paddingLeft: "24px",
    paddingRight: "24px"
  },
  iconStyles : {
    marginLeft: 20,
    marginRight: 10
  }
}
class AppNavigation extends Component {

  constructor(props) {
    super(props);
    this.state = {open: false};
    this.handleToggle = this.handleToggle.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleToggle() {
    this.setState({open: !this.state.open});
  }

  handleClose() {
     this.setState({open: false});
  }

  render() {
    const { authenticated } = this.props;
    const { open } = this.state;
    return(
      <div>
      <AppBar
        style={styles.barStyle}
        title="Todo App"
        iconElementLeft={
          <IconButton onTouchTap={this.handleToggle}>
            <NavigationApps />
          </IconButton>
        }
        />
        { authenticated ? <AuthenticatedNavigation status={open} handleClose={this.handleClose}/> : <PublicNavigation status={open} handleClose={this.handleClose}/> }
      </div>
    )
  }
}
export default AppNavigation;
