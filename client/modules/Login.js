import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form/immutable'; // <--- immutable import
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import Formsy from 'formsy-react';
import { FormsyCheckbox, FormsyText } from 'formsy-material-ui/lib';


const styles = {
  section: {
    paddingTop: "64px",
    minHeight: "480px"
  },
  buttonStyle: {
    margin: 12
  },
  paperStyle: {
      width: 300,
      margin: 'auto'
  },
  submitStyle: {
    marginTop: 32
  }
}
class Login extends Component {
  constructor(props) {
    super(props);
    console.log('Login', this.props);
    this.state = {
      canSubmit: false
    };
    this.enableButton = this.enableButton.bind(this);
    this.disableButton = this.disableButton.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.notifyFormError = this.notifyFormError.bind(this);
  }
  enableButton() {
    this.setState({
      canSubmit: true
    });
  }

  disableButton() {
    this.setState({
      canSubmit: false,
    });
  }

  submitForm(data) {
    //alert(JSON.stringify(data, null, 4));
    this.props.signInHandler();
  }

  notifyFormError(data) {
    console.error('Form error:', data);
  }
  render() {
    return(
      <div style={styles.section}>
        <Paper style={styles.paperStyle}>
          <Formsy.Form
            onValid={this.enableButton}
            onInvalid={this.disableButton}
            onValidSubmit={this.submitForm}
            onInvalidSubmit={this.notifyFormError}
            >
            <FormsyText
              name="email"
              validations="isEmail"
              validationError="This is not a valid email"
              required
              hintText="What is your email id?"
              floatingLabelText="Email Id"
              />
              <FormsyText
                name="password"
                type="password"
                required
                hintText="What is your password?"
                floatingLabelText="Password"
                />
              <RaisedButton
                style={styles.submitStyle}
                type="submit"
                label="Submit"
                primary={true}
                disabled={!this.state.canSubmit}
                />
            </Formsy.Form>
        </Paper>
      </div>
    )
  }
}

export default Login;
