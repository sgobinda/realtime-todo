import React, { Component } from 'react';

const styles = {
  section: {
    paddingTop: "64px",
    minHeight: "480px"
  }
}

class IndexPage extends Component {
  render() {
    return(<div style={styles.section}>IndexPage</div>)
  }
}

export default IndexPage;
