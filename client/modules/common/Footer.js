import React, { Component } from 'react';
import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation';
import Paper from 'material-ui/Paper';

const styles = {
  footer : {
    bottom: 0,
    backgroundColor: "rgb(0, 188, 212)"
  }
}
class Footer extends Component {
  constructor(props) {
    super(props);
  }

  render(){
    return(
      <Paper zDepth={2} >
        <BottomNavigation style={styles.footer}/>
      </Paper>
    )
  }
}
export default Footer;
