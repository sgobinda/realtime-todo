import React, { Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as authActions from '../actions/authActions';
import Login from '../modules/Login';

class LoginContainer extends Component {

  constructor(props) {
    super(props);
    console.log("LoginContainer", this.props);
    this.signInHandler = this.signInHandler.bind(this);
  }

  signInHandler() {
    this.props.actions.login();
    this.props.history.push("/home");
  }

  render(){
    return(
      <Login signInHandler={this.signInHandler}/>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {actions: bindActionCreators(Object.assign(authActions), dispatch)}
}

export default connect(null, mapDispatchToProps)(LoginContainer);
