import {
  SIGN_IN_REQUEST,
  SIGN_OUT_REQUEST
} from './actionTypes';

export function login() {
  return {
    type: SIGN_IN_REQUEST,
    payload: true
  }
}

export function logout() {
  return {
    type: SIGN_OUT_REQUEST,
    payload: false
  }
}
