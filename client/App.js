import React from 'react';
import PropTypes from 'prop-types';
import { ConnectedRouter } from 'connected-react-router';
import AppRoutes from './routes/AppRoutes';

const App = ({history}) => {
  return(
    <ConnectedRouter history={history}>
      <AppRoutes />
    </ConnectedRouter>
  )
}

App.propTypes = {
  history: PropTypes.object,
}

export default App;
